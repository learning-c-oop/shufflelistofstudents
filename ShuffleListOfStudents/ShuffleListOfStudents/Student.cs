﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShuffleListOfStudents
{
    class Student
    {
        public Student(string name, string surname, string email, int age)
        {
            if (!string.IsNullOrEmpty(name))
            {
                this.Name = name;
            }
            if (!string.IsNullOrEmpty(surname))
            {
                this.Surname = surname;
            }
            if (!string.IsNullOrEmpty(email))
            {
                this.Email = email;
            }
            this.Age = age;
        }
        public string Name
        { get; set; }
        public string Surname
        { get; set; }
        public string Email
        { get; set; }
        public int Age
        { get; set; }
    }
}
