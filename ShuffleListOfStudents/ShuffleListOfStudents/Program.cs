﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShuffleListOfStudents
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> list = CreateListOfStudents(5);
            PrintListOfStudents(list);
            Console.WriteLine();
            ShuffleListOfStudents(list);
            PrintListOfStudents(list);

            Console.ReadKey();
        }
        static List<Student> CreateListOfStudents(int count)
        {
            List<Student> list = new List<Student>(count);
            for (int i = 0; i < count; i++)
            {
                Student student = new Student(
                name: $"A{i + 1}",
                surname: $"A{i + 1}yan",
                email: $"A{i + 1}@gmail.com",
                age: 20 + i + 1
                );
                list.Add(student);
            }
            return list;
        }
        static void ShuffleListOfStudents(List<Student> list)
        {
            Random rnd = new Random();
            for (int i = 0; i < list.Count; i++)
            {
                List<Student> temp = new List<Student>();
                int rndNum = rnd.Next(0, list.Count);
                temp.Add(list[i]);
                list[i] = list[rndNum];
                list[rndNum] = temp[0];
            }
        }
        static void PrintListOfStudents(List<Student> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine($"Name: {list[i].Name} , Surname: {list[i].Surname} , Email: {list[i].Email} , Age: {list[i].Age}");
            }
        }
    }
}
